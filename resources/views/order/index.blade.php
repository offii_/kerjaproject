@extends('layouts.app')
@section('content')
<section class="ftco-section main-panel">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute bg-primary fixed-top">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <div class="navbar-toggle">
                        <button type="button" class="navbar-toggler">
                            <span class="navbar-toggler-bar bar1"></span>
                            <span class="navbar-toggler-bar bar2"></span>
                            <span class="navbar-toggler-bar bar3"></span>
                        </button>
                    </div>
                    <a class="navbar-brand" href="#pablo">Transaction</a>
                </div>
            </div>
        </nav>
        <div class="row">
            <div class="col-md-12 ftco-animate">
                <div class="row justify-content-center">
                    <div class="col-lg-4 mt-5 cart-wrap ftco-animate">
                        <form role="form" action="{{ route('order.create')}}" method="post">
                            @csrf
                            <div class="cart-total mb-3">
                                <div class="form-group">
                                    <label for="">Customer Name</label>
                                    <input type="text" class="form-control text-left px-3" name="name" value="{{@$data->name}}" placeholder="" required>
                                    <label for="">Table Number</label>
                                    <select name="table" id="table" class="form-control" required>
                                        <option value=""> -- Select Table Number --</option>
                                        @foreach ($tables as $table)
                                            <option value="{{ $table->id }}">{{ $table->number }}</option>
                                        @endforeach 
                                    </select>
                                </div>
                            </div>
                            <button class="btn btn-primary py-3 px-4">Select Product</button>
                        </form>
                    </div>
                </div>
                </br>
                <div class="col-md-8 ftco-animate">
                <a href="/export_excel" class="btn btn-success my-3" target="_blank">EXPORT EXCEL</a>
                        <table class="table">
                            <thead class="thead-primary">
                            <tr class="text-center">
                                <th>Id</th>
                                <th>Customer Name</th>
                                <th>Table Number</th>
                                <th>Total Item</th>
                                <th>Total Price</th>
                                <th>Cash</th>
                                <th>Change</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            @foreach ($datas as $data)
                            <tr class="text-center">
                                <td>{{$data->id}}</td>
                                <td>{{$data->customer_name}}</td>
                                <td>{{$data->table_number}}</td>
                                <td>{{$data->total_item}}</td>
                                <td>{{$data->total_price}}</td>
                                <td>{{$data->cash}}</td>
                                <td>{{$data->change}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div> 
</section>
@endsection