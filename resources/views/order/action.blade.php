<form action="{{ route($view.'.destroy', $c->order_id)}}" method="POST">
	@csrf
	@method("DELETE")

	<td>
		<a href="{{ route($view. '.edit', $c->order_id)}}" class="btn btn-warning">
			Edit
		</a>
		<button type="submit" class="btn btn-danger">
			Delete
		</button>

	</td>

</form>