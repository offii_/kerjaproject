@extends('layouts.app')
@section('content')
<section class="ftco-section main-panel">
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute bg-primary fixed-top">
			<div class="container-fluid">
				<div class="navbar-wrapper">
					<div class="navbar-toggle">
						<button type="button" class="navbar-toggler">
							<span class="navbar-toggler-bar bar1"></span>
							<span class="navbar-toggler-bar bar2"></span>
							<span class="navbar-toggler-bar bar3"></span>
						</button>
					</div>
					<a class="navbar-brand" href="#pablo">Transaction</a><a class="navbar-brand"> > Checkout</a>
				</div>
			</div>
		</nav>
		@foreach ($datas as $p)
		<div class="customer">
			<table>
				<tr>
					<td>Customer Name</td>
					<td> : </td>
					<td>{{$p->customer_name}}</td>
				</tr>
				<tr>
					<td>Table Number</td>
					<td> : </td>
					<td>{{$p->table_number}}</td>
				</tr>
			</table>
		</div>
		@endforeach
		<div class="row">
			<div class="col-md-12 ftco-animate">
				<div class="cart-list">
					<table class="table">
						<thead class="thead-primary">
						<tr class="text-center">
							<th>&nbsp;</th>
							<th>name</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
							<th>Action</th>
						</tr>
						</thead>
						<tbody>
						
						@php
						function getId($url) {

										$x_array = explode('=', $url);
										@$x_id = $x_array[1];

								return $x_id;
						}
						@endphp
						@foreach($carts as $c)
						<tr class="text-center">
							<td class="image-prod"><div class="img" style="background-image:url(https://drive.google.com/uc?id={{ getId($c->image) }});"></div></td>
							
							<td class="product-name">
								<h3>{{$c->name}}</h3>
							</td>
							
							<td class="price">{{$c->selling_price}}</td>
							
							<td class="quantity">{{$c->qty}}</td>
							
							<td class="total">{{$c->selling_price * $c->qty}}</td>
							@include('order.action')
						</tr><!-- END TR-->
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		@foreach($carts as $c)
		<form role="form" action="{{ route('order.checkout',$c->orders_id)}}" method="post">
			@csrf
			@endforeach
    		<div class="col-lg-4 mt-5">
    			<div class="cart-total mb-3">
    				<h3>Cart Totals</h3>
    				<p class="d-flex">
						<span>Total Item</span>
						<span>{{$total_item}}</span>
							<input type="number" id="total_item" name="total_item" value="{{$total_item}}" hidden>
					</p>
					<p class="d-flex">
						<span>Total</span>
						<span>{{$total_price}}</span>
							<input type="number" id="price" name="price" value="{{$total_price}}" hidden>
					</p>
						<p class="d-flex">
						<span>Cash</span>
						<input style="width:140px;" type="number" id="cash"  min="{{$total_price}}" name="cash" onkeyup="count()">
					</p>
					<hr>
					<p class="d-flex total-price">
						<span>Change</span>
						<input style="width:140px;" class="bg-secondary" min="1" type="number" id="change" name="change" readonly>
					</p>
    			</div>
				<p><button class="btn btn-primary py-3 px-4">Proceed to Checkout</button></p>
			</div>
		</form>
	</div>
</section>

<script>
	function count(){
		var v_price = document.getElementById('price').value;
		var v_cash = document.getElementById('cash').value;
		var result = v_cash - v_price;

		document.getElementById('change').value = result;
		}
</script>
@endsection