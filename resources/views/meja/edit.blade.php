@extends('layouts.app')

@section('content')

    <!-- /.card-header -->
    <!-- form start -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tbody>
                    <div class="row justify-content-center  ">
                        <div class="row">
                        <style>
                        .tengah{
                        position: center;
                        margin-top: 60px;
                        margin-left: 200px;
                        left: 50%;
                        top: 50%;
                        }
                        </style>
                        <div class="tengah">
                            <div class="col-lg-12 mt-12 cart-wrap ">
                                <form class="main-panel" role="form" action="{{ route('meja.update', $datas->id)}}" method="post">
                                    @method('PUT')
                                    @csrf
                                    
                                    <div class="cart-total mb-11">
                                        <div class="form-group">
                                            <h4>EDIT STATUS TABLE</h4>
                                            <div class="form-group">
                                                <label for="">Number Table</label>
                                                <input type="text" readonly class="form-control text-left px-3" name="number" value="{{@$datas->number}}" placeholder="">
                                            </div>
                                            <div class="form-group">
                                                <label for="">Status</label>
                                                <select name="active" id="active" value="active" class="form-control" required>
                                                    <option value="1" {{@$datas->active == '1'  ? 'selected' : ''}}>Active</option>
                                                    <option value="2" {{@$datas->active == '0'  ? 'selected' : ''}}>Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <p><button type="submit" class="btn btn-primary py-3 px-4">Update</button></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </tbody>
            </table>
        </div>
    </div>
</div>