<form action="{{ route($view.'.destroy', $data->id)}}" method="POST">
	@csrf
	@method("DELETE")

	<td>
		<a href="{{ route($view. '.edit', $data->id)}}" class="btn btn-warning">
			Edit
		</a>
		<button type="submit" class="btn btn-danger">
			Delete
		</button>

	</td>

</form>