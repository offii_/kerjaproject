@extends('layouts.app')

@section('content')

    <!-- /.card-header -->
    <!-- form start -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tbody>
                    <div class="row justify-content-center  ">
                        <div class="row">
                        <style>
                        .tengah{
                        position: center;
                        margin-top: 60px;
                        margin-left: 200px;
                        left: 50%;
                        top: 50%;
                        }
                        </style>
                        <div class="tengah">
                            <div class="col-lg-12 mt-12 cart-wrap ">
                                <form class="main-panel" role="form" action="{{ route('category.store')}}" method="post">
                                    @csrf
                                    
                                    @include('category.field')
                                    
                                    <div class="cart-total mb-11">
                                        <div class="form-group">
                                            <h4>ADD CATEGORY</h4>
                                            <div>
                                                <label for="">Parent Id</label>
                                                <input type="text" class="form-control text-left px-3" name="parent_id" value="{{ @$data->parent_id }}" placeholder="">
                                            </div>
                                            <div>
                                                <label for="">Name</label>
                                                <input type="text" class="form-control text-left px-3" name="name" value="{{ @$data->name }}" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <p><button type="submit" class="btn btn-primary py-3 px-4">Submit</button></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </tbody>
            </table>
        </div>
    </div>
</div>
