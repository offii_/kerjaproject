<section class="ftco-section main-panel">
			<div class="container">
      <nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute bg-primary fixed-top">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                        <a class="navbar-brand" href="#pablo">Category</a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                        <span class="navbar-toggler-bar navbar-kebab"></span>
                    </button>
                </div>
            </nav>
				<div class="row">
    			<div class="col-md-12 ftco-animate">
    				<div class="cart-list">
	    				<table class="table">
						    <tbody>
                            <div class="row justify-content-center  ">
                                <div class="row">
                                    <div class="col-md-8 ftco-animate">
                                            <table class="table">
                                                <thead class="thead-primary">
                                                <tr class="text-center">
                                                    <th>Id</th>
                                                    <th>Parent Id</th>
                                                    <th>Name</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                @foreach ($datas as $data)
                                                <tr class="text-center">
                                                    <td>{{$data->id}}</td>
                                                    <td>{{ @$data->parent->name }}</td>
                                                    <td>{{ @$data->name}}</td>
                                                    @include('category.action')
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
    		                      </div>

						    </tbody>
						  </table>
					  </div>
    			</div>
    		</div>
            
		</section>
@endsection
