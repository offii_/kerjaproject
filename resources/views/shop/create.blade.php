@extends('layouts.app')
@section('content')
<section class="ftco-section main-panel">
    <div class="container">
		<nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute bg-primary fixed-top">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                        <a class="navbar-brand" href="#pablo">Transaction</a><a class="navbar-brand"> > Select Product</a>
                    </div>
                </div>
            </nav>
			<form role="form" action="{{ route('shop.create')}}" method="post">
            @csrf

			@foreach ($customer as $p)
			<input type="text" name="order_id" value="{{$p->id}}" hidden>
			<div class="customer">
			<table>
				<tr>
					<td>Customer Name</td>
					<td> : </td>
					<td>{{$p->customer_name}}</td>
				</tr>
				<tr>
					<td>Table Number</td>
					<td> : </td>
					<td>{{$p->table_number}}</td>
				</tr>
			</table>
			</div>
			@endforeach
    		<div class="row">
			@php
			function getId($url) {

					$x_array = explode('=', $url);
					@$x_id = $x_array[1];

				return $x_id;
			}
			@endphp
			@foreach($products as $product)
			<input type="text" name="product_id" value="{{$product->id}}" hidden>
			<input type="text" name="price" value="{{$product->selling_price}}" hidden>
    			<div class="col-lg-6 mb-5 ftco-animate">
    				<a href="https://drive.google.com/uc?id={{ getId(@$product->image) }}" class="image-popup"><img src="https://drive.google.com/uc?id={{ getId(@$product->image) }}" class="img-fluid" alt="Colorlib Template"></a>
    			</div>
    			<div class="col-lg-6 product-details pl-md-5 ftco-animate">
    				<h3>{{@$product->name}}</h3>
    				<p style="color: #000;">Rp. {{@$product->selling_price}}</p>
	             	<input class="col-6" type="number" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="{{@$product->stock}}">
					<p class="mt-3" style="color: #000;">{{@$product->stock}} Available</p>
					<p><a class="btn btn-black py-3 px-5"><button type="submit">Add to Cart</button></a></p>
    			</div>
				@endforeach
    		</div>
			</form>
    	</div>
    </section>
@endsection