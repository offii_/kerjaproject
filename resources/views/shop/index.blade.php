@extends('layouts.app')
@section('content')
    <section class="ftco-section main-panel">
    	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute bg-primary fixed-top">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <div class="navbar-toggle">
                            <button type="button" class="navbar-toggler">
                                <span class="navbar-toggler-bar bar1"></span>
                                <span class="navbar-toggler-bar bar2"></span>
                                <span class="navbar-toggler-bar bar3"></span>
                            </button>
                        </div>
                        <a class="navbar-brand" href="#pablo">Transaction</a><a class="navbar-brand"> > Select Product</a>
                    </div>
					@foreach ($customer as $p)
					<a href="{{ route('cart.index', $p->id)}}" class="nav-link"><span class="icon-shopping_cart"></span></a>
					@endforeach
                </div>
            </nav>
			@foreach ($customer as $p)
			<div class="customer">
			<table>
				<tr>
					<td>Customer Name</td>
					<td> : </td>
					<td>{{$p->customer_name}}</td>
				</tr>
				<tr>
					<td>Table Number</td>
					<td> : </td>
					<td>{{$p->table_number}}</td>
				</tr>
			</table>
			</div>
			@endforeach
			@php
			function getId($url) {

					$x_array = explode('=', $url);
					@$x_id = $x_array[1];

				return $x_id;
			}
			@endphp
    		<div class="row">
			@foreach($product as $product)
    			<div class="col-md-6 col-lg-3 ftco-animate">
    				<div class="product">
    					<a href="#" class="img-prod"><img class="img-fluid" src="https://drive.google.com/uc?id={{ getId($product->image) }}" alt="asd">
    						<div class="overlay"></div>
    					</a>
    					<div class="text py-3 pb-4 px-3 text-center">
    						<h3><a href="#">{{$product->name}}</a></h3>
    						<div class="d-flex">
    							<div class="pricing">
		    						<p class="price"><span class="price-sale">{{$product->selling_price}}</span></p>
		    					</div>
	    					</div>
							<div class="bottom-area d-flex px-3">
	    						<div class="m-auto d-flex">
	    							<a href="{{ route('shop.store', $product->id)}}" class="buy-now d-flex justify-content-center align-items-center mx-1">
	    								<span><i class="ion-ios-cart"></i></span>
	    							</a>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
				@endforeach
    		</div>
    		<!-- <div class="row mt-5">
          <div class="col text-center">
            <div class="block-27">
              <ul>
                <li><a href="#">&lt;</a></li>
                <li class="active"><span>1</span></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&gt;</a></li>
              </ul>
            </div>
          </div>
        </div> -->
    	</div>
    </section>
@endsection
    