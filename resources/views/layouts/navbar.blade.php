        <div class="sidebar" data-color="green">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->
            <div class="logo">
                <a href="#" class="simple-text logo-mini "></a>
                <a href="{{ route('order.index')}}" class="simple-text logo-normal justify-content-center">
				Wikrama Caffe
				</a>
				<!-- <a class="navbar-brand" href="{{ route('order.index')}}">Wikrama Caffe</a> -->
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li>
                        <a href="{{ route('order.index')}}">
                            <i class="now-ui-icons shopping_cart-simple"></i>
                            <p>Transaction</p>
                        </a>
					</li>
					<li class="dropdown-btn">
					<a>
								<i class="now-ui-icons education_atom"></i>
								<p>Manage</p>
							</a>
					</li>
					<div  class="dropdown-container">
						<li>
							<a href="{{ route('unit.index') }}">
								<i class="now-ui-icons business_money-coins"></i>
								<p>Unit</p>
							</a>
						</li>
						<li>
							<a href="{{ route('category.index') }}">
								<i class="now-ui-icons business_chart-pie-36"></i>
								<p>Category</p>
							</a>
						</li>
						<li>
							<a href="{{ route('product.index') }}">
								<i class="now-ui-icons design_app"></i>
								<p>Product</p>
							</a>
						</li>
						<li>
							<a href="{{ route('meja.index') }}">
								<i class="now-ui-icons business_badge"></i>
								<p>Table</p>
							</a>
						</li>
					</div>
					<li>
                        <a href="{{ url('/logout') }}">
                            <i class=""></i>
                            <p>Logout</p>
                        </a>
					</li>
                </ul>
            </div>
        </div>
        
        </div>