@extends('layouts.app')
@section('content')
<section class="ftco-section main-panel">
			<div class="container">
      <nav class="navbar navbar-expand-lg navbar-transparent  navbar-absolute bg-primary fixed-top">
        <div class="container-fluid">
            <div class="navbar-wrapper">
                <div class="navbar-toggle">
                    <button type="button" class="navbar-toggler">
                        <span class="navbar-toggler-bar bar1"></span>
                        <span class="navbar-toggler-bar bar2"></span>
                        <span class="navbar-toggler-bar bar3"></span>
                    </button>
                </div>
                <a class="navbar-brand" href="#pablo">Product</a>
            </div>
        </div>
    </nav>
				<div class="row">
    			<div class="col-md-12 ftco-animate">
    				<div class="cart-list">
            @if(session('message'))
              <p>{{ session('message') }}</p>
            @endif
	    				<table class="table">

						    <tbody>
							
                            <div class="row justify-content-center  ">
                            <a href="{{ route($view. '.create') }}" class="btn btn-primary"> Add </a>

                                <div class="row">
                                    <div class="col-md-8 ftco-animate">
                                            <table class="table">
                                                <thead class="thead-primary">
                                                <tr class="text-center">
                                                    <th>Id</th>
                                                    <th>Name</th>
                                                    <th>Category</th>
                                                    <th>Image</th>
                                                    <th>Description</th>
                                                    <th>Price</th>
                                                    <th>Stock</th>
                                                    <th>Unit</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php
                                                function getId($url) {

                                                        $x_array = explode('=', $url);
                                                        @$x_id = $x_array[1];

                                                    return $x_id;
                                                }
                                                @endphp
                                                @foreach ($datas as $data)
                                                <tr class="text-center">
                                                    <td>{{$data->id}}</td>
                                                    <td >{{$data->name}}</td>
                                                    <td >{{$data->category->name}}</td>
                                                    <td ><img src="https://drive.google.com/uc?id={{ getId($data->image) }}" alt="asd"></td>
                                                    <td >{{$data->description}}</td>
                                                    <td >{{$data->selling_price}}</td>
                                                    <td >{{$data->stock}}</td>
                                                    <td >{{$data->unit->name}}</td>
                                                    @if ($data->status === 1)
                                                    <td >Active</td>
                                                    @else
                                                    <td >Inactive</td>
                                                    @endif
                                                    @include('unit.action')
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
    		</div>

						    </tbody>
						  </table>
					  </div>
    			</div>
    		</div>
		</section>
@endsection