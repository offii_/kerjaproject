@extends('layouts.app')

@section('content')

    <!-- /.card-header -->
    <!-- form start -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tbody>
                    <div class="row justify-content-center  ">
                        <div class="row">
                        <style>
                        .tengah{
                        position: center;
                        margin-top: 60px;
                        margin-left: 200px;
                        left: 50%;
                        top: 50%;
                        }
                        </style>
                        <div class="tengah">
                            <div class="col-lg-12 mt-12 cart-wrap ">
                                <form class="main-panel" role="form" action="{{ route('product.store')}}" method="post">
                                    @csrf
                                    
                                    @include('unit.field')
                                    
                                    <div class="cart-total mb-11">
                                        <div class="form-group">
                                            <h4>ADD PRODUCT</h4>
                                            <label for="">Product Name</label>
                                            <input type="text" class="form-control text-left px-3" name="name" value="{{@$data->name}}" placeholder="" required>
                                            <label for="">Image</label>
                                            <input type="text" class="form-control text-left px-3" name="image" value="{{@$data->image}}" placeholder="image link from drive" required>
                                            <label for="">Category</label>
                                            <select name="category" id="category" class="form-control" required>
                                                <option value=""> -- Select Category --</option>
                                                @foreach ($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach 
                                            </select>
                                            <label for="">Purchase Price</label>
                                            <input type="number" class="form-control text-left px-3" name="pprice" value="{{@$data->pprice}}" placeholder="" required>
                                            <label for="">Selling Price</label>
                                            <input type="number" class="form-control text-left px-3" name="sprice" value="{{@$data->sprice}}" placeholder="" required>
                                            <label for="">Stock</label>
                                            <input type="number" class="form-control text-left px-3" name="stock" value="{{@$data->stock}}" placeholder="" required>
                                            <label for="">Dexcription</label>
                                            <input type="textarea" class="form-control text-left px-3" name="description" value="{{@$data->description}}" placeholder="" required>
                                            <label for="">Unit</label>
                                            <select name="unit" id="unit" class="form-control" required>
                                                <option value=""> -- Select Unit --</option>
                                                @foreach ($units as $unit)
                                                    <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                                                @endforeach 
                                            </select>
                                            <label for="">Status</label>
                                            <select name="status" id="status" class="form-control" required>
                                                <option value="1">Active</option>
                                                <option value="2">Inactive</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <p><button type="submit" class="btn btn-primary py-3 px-4">Submit</button></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </tbody>
            </table>
        </div>
    </div>
</div>
