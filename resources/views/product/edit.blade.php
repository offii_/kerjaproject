@extends('layouts.app')

@section('content')

    <!-- /.card-header -->
    <!-- form start -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tbody>
                    <div class="row justify-content-center  ">
                        <div class="row">
                        <style>
                        .tengah{
                        position: center;
                        margin-top: 60px;
                        margin-left: 200px;
                        left: 50%;
                        top: 50%;
                        }
                        </style>
                        <div class="tengah">
                            <div class="col-lg-12 mt-12 cart-wrap ">
                                <form class="main-panel" role="form" action="{{ route('product.update', $datas->id)}}" method="post">
                                    @method('PUT')
                                    @csrf
                                    
                                    <div class="cart-total mb-11">
                                        <div class="form-group">
                                            <h4>EDIT UNIT</h4>
                                            <label for="">Product Name</label>
                                            <input type="text" class="form-control text-left px-3" name="name" value="{{@$datas->name}}" placeholder="">
                                            <label for="">Image</label>
                                            <input type="text" class="form-control text-left px-3" name="image" value="{{@$datas->image}}" placeholder="">
                                            <label for="">Category</label>
                                            <select name="category" id="category" class="form-control">
                                                <option value=""> -- Select Category --</option>
                                                @foreach ($categories as $category)
                                                    <option value="{{ $category->id }}" {{ (isset($category->id) || old('id'))? "selected":""}}>{{ $category->name }}</option>
                                                @endforeach 
                                            </select>
                                            <label for="">Purchase Price</label>
                                            <input type="number" class="form-control text-left px-3" name="pprice" value="{{@$datas->purchase_price}}" placeholder="">
                                            <label for="">Selling Price</label>
                                            <input type="number" class="form-control text-left px-3" name="sprice" value="{{@$datas->selling_price}}" placeholder="">
                                            <label for="">Stock</label>
                                            <input type="number" class="form-control text-left px-3" name="stock" value="{{@$datas->stock}}" placeholder="">
                                            <label for="">Dexcription</label>
                                            <input type="textarea" class="form-control text-left px-3" name="description" value="{{@$datas->description}}" placeholder="">
                                            <label for="">Unit</label>
                                            <select name="unit" id="unit" class="form-control">
                                                <option value=""> -- Select Unit --</option>
                                                @foreach ($units as $unit)
                                                    <option value="{{ $unit->id }}" {{ (isset($unit->id) || old('id'))? "selected":""}}>{{ $unit->name }}</option>
                                                @endforeach 
                                            </select>
                                            <label for="">Status</label>
                                            <select name="active" id="active" class="form-control">
                                                <option value="1">Active</option>
                                                <option value="2">Inactive</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <p><button type="submit" class="btn btn-primary py-3 px-4">Update</button></p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        </div>
                    </div>
                </tbody>
            </table>
        </div>
    </div>
</div>