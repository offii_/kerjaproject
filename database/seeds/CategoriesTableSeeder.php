<?php
use App\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
            [
                [
                    'parent_id' => '1',
                    'name' => 'Minuman'
                ],
                [
                    'parent_id' => '2',
                    'name' => 'Makanan'
                ],
                [
                    'parent_id' => '2',
                    'name' => 'Kue'
                ],
                [
                    'parent_id' => '2',
                    'name' => 'Gorengan'
                ],
                [
                    'parent_id' => '1',
                    'name' => 'Juice'
                ]
            ]
        );
    }
}
