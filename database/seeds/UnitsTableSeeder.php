<?php
use App\Unit;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert(
            [
                [
                    'name' => 'Gelas'
                ],
                [
                    'name' => 'Piring'
                ],
                [
                    'name' => 'Mangkuk'
                ],
                [
                    'name' => 'Tusuk'
                ],
                [
                    'name' => 'Buah'
                ]
            ]
        );
    }
}
