<?php
use App\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
        [
            [
                'category_id' => '1',
                'name' => 'Teh Tarik',
                'description' => 'Cool or Hot',
                'image' => 'https://drive.google.com/open?id=1AKN-hnWpZaJljMOwM6l2tiAnsoq75LPU',
                'purchase_price' => '3000',
                'selling_price' => '5000',
                'stock' => '10',
                'unit_id' => '1'
            ],
            [
                'category_id' => '5',
                'name' => 'Juice Mangga',
                'description' => 'Cool',
                'image' => 'https://drive.google.com/open?id=1Ekp3XIlbSZlclnla7OOxBkzOzYU-oGhu',
                'purchase_price' => '4500',
                'selling_price' => '6000',
                'stock' => '7',
                'unit_id' => '1'
            ], 
            [
                'category_id' => '5',
                'name' => 'Coffe',
                'description' => 'Cool or Hot',
                'image' => 'https://drive.google.com/open?id=1kca2sLDa16WtePLfkJRiOtBe5Ayap16j',
                'purchase_price' => '3000',
                'selling_price' => '5000',
                'stock' => '15',
                'unit_id' => '1'
            ],
            [
                'category_id' => '3',
                'name' => 'Sus',
                'description' => 'isi Vla',
                'image' => 'https://drive.google.com/open?id=1q2e87V78BOmUfQIY1YnrkFEemLsYq1S5',
                'purchase_price' => '3000',
                'selling_price' => '3500',
                'stock' => '25',
                'unit_id' => '5'
            ],
            [
                'category_id' => '3',
                'name' => 'Dadar Gulung',
                'description' => 'isi Vla',
                'image' => 'https://drive.google.com/open?id=1-jz4liVX0KFGIBvzzarjYu_YmhzkMq5O',
                'purchase_price' => '1500',
                'selling_price' => '2000',
                'stock' => '10',
                'unit_id' => '5'
            ], 
            [
                'category_id' => '2',
                'name' => 'Pudding',
                'description' => 'Random variant',
                'image' => 'https://drive.google.com/open?id=11B02QQB57FkywCiiZTGwFFrA73D1GEJR',
                'purchase_price' => '4000',
                'selling_price' => '5000',
                'stock' => '5',
                'unit_id' => '3'
            ],
            [
                'category_id' => '4',
                'name' => 'Cireng',
                'description' => 'isi ayam',
                'image' => 'https://drive.google.com/open?id=1XwkTdfIv0C6T2PEMsbBfCzYiVYgVUTd3',
                'purchase_price' => '1700',
                'selling_price' => '2000',
                'stock' => '35',
                'unit_id' => '5'
            ],
            [
                'category_id' => '4',
                'name' => 'Risol',
                'description' => 'isi sayur',
                'image' => 'https://drive.google.com/open?id=176C3uK6eZ1O9pIrjpk1N8VUTc5Jlzi4M',
                'purchase_price' => '2500',
                'selling_price' => '3000',
                'stock' => '20',
                'unit_id' => '5'
            ], 
            [
                'category_id' => '4',
                'name' => 'Sosis Solo',
                'description' => 'isi ayam',
                'image' => 'https://drive.google.com/open?id=1fMdPGG_o-N92O-YFjpaPC9EC2wEhBoKA',
                'purchase_price' => '2800',
                'selling_price' => '3000',
                'stock' => '15',
                'unit_id' => '5'
            ],
            [
                'category_id' => '2',
                'name' => 'Cilok',
                'description' => 'Bumbu kacang',
                'image' => 'https://drive.google.com/open?id=1e0qKX5T5dTTVXsN5_X0Q2CPabIqnNN_V',
                'purchase_price' => '1500',
                'selling_price' => '2000',
                'stock' => '32',
                'unit_id' => '4'
            ], 
            [
                'category_id' => '2',
                'name' => 'Sosis Bakar',
                'description' => 'Bakar',
                'image' => 'https://drive.google.com/open?id=1lJkKpiWVpYynManQdiNfN_YJlEhu7R9Q',
                'purchase_price' => '1500',
                'selling_price' => '2000',
                'stock' => '30',
                'unit_id' => '4'
            ],
            [
                'category_id' => '2',
                'name' => 'Bakso Bakar',
                'description' => 'Bakar',
                'image' => 'https://drive.google.com/open?id=1wgce1jY79XFrPu5G5hzHeOcLoYVFg0z5',
                'purchase_price' => '1500',
                'selling_price' => '2000',
                'stock' => '30',
                'unit_id' => '4'
            ]
        ]
        );
    }
}
