<?php

use Illuminate\Database\Seeder;

class MejaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('meja')->insert(
            [
                [
                    'number' => '1',
                ],
                [
                    'number' => '2',
                ],
                [
                    'number' => '3',
                ],
                [
                    'number' => '4',
                ],
                [
                    'number' => '5',
                ]
            ]
        );
    }
}
