<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailOrder extends Model
{
    protected $fillable = ['id', 'product_id', 'qty', 'price', 'subtotal'];
    public $table = "order_details";
    public function product() {
        return $this->belongsTo('App\Product', 'product_id', 'id');
    }

}
