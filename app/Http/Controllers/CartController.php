<?php

namespace App\Http\Controllers;
use App\Unit;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class CartController extends Controller
{
    
    private $title,$view;

    public function __construct(
        Unit $model
    ) {
        $this->model = $model;

        $this->title    = "Cart";
        $this->view     = "cart";

        view()->share('title', $this->title);
        view()->share('view', $this->view);
      }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $subtotal = 0;
        // $cart = $request->session()->get('cart');

        // $subtotal = 0;
        // foreach($cart as $k => $v){
        //     $product = Product::whereId($k)->first();
        //     $cart[$k] = ['product' => $product, $v];
           
        //     $subtotal += ($product->selling_price * $v);
        // }
        // return $cart;
        return view($this->view. '.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view($this->view.'.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input  = $request->all();
        unset($input['_token']);

        $this->model->create($input);

        return redirect()->route($this->view.'.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->model->findOrFail($id);

        return view($this->view . '.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->model->findOrFail($id);

        return view($this->view . '.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $data = $this->model->findOrFail($id);
        $data->update($input);

        return redirect()->route($this->view . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data= $this->model->findOrFail($id);
        $data->delete();

        return redirect()->route($this->view . '.index');
    }

    public function addToCart(Request $request , $product_id) {
        $cart = $request->session()->get('cart');
        
        if(!isset($cart[$product_id])) {
            $cart[$product_id] = 1;
        }
        $request->session()->put('cart', $cart);

        return redirect()->route($this->view . '.index');
    }
}
