<?php

namespace App\Http\Controllers;
use App\Unit;
use App\Product;
use App\Order;
use App\DetailOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ShopController extends Controller
{
    
    private $title,$view;

    public function __construct(
        Product $model
    ) {
        $this->model = $model;

        $this->title    = "Shop";
        $this->view     = "shop";

        view()->share('title', $this->title);
        view()->share('view', $this->view);
      }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = $this->view;
        return view($this->view. '.index', compact('datas'));

    }

  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $product = new DetailOrder($request->all());
        $price = $request->price;
        $quantity = $request->quantity;
        $subtotal = $price * $quantity;
        $product->order_id = $request->order_id;
        $product->product_id = $request->product_id;
        $product->price = $price;
        $product->qty = $quantity;
        $product->subtotal = $subtotal;
        $product->save();

        return redirect()->route('order.creates');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        $products = $this->model->with(['category', 'unit'])->where('id',$id)->get();
        $customer_id = Order::max('id');
        $customer = Order::where('id', '=', $customer_id)->get();
        return view('shop.create', compact('products','customer'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->model->findOrFail($id);

        return view('pages.' .$this->view . '.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->model->findOrFail($id);

        return view('pages.' .$this->view . '.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $data = $this->model->findOrFail($id);
        $data->update($input);

        return redirect()->route($this->view . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data= $this->model->findOrFail($id);
        $data->delete();

        return redirect()->route($this->view . '.index');
    }
}
