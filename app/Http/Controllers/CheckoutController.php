<?php

namespace App\Http\Controllers;
use App\Unit;
use App\Order;
use App\Product;
use App\DetailOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CheckoutController extends Controller
{
    
    private $title,$view;

    public function __construct(
        Unit $model,
        Order $model_order,
        Product $model_product,
        DetailOrder $model_detail_order
    ) {
        $this->model = $model;
        $this->model_order = $model_order;
        $this->model_product = $model_product;
        $this->model_detail_order = $model_detail_order;

        $this->title    = "Checkout";
        $this->view     = "checkout";

        view()->share('title', $this->title);
        view()->share('view', $this->view);
      }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datas = $this->view;
        return view($this->view. '.index', compact('datas'));

    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('pages.'.$this->view.'.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $carts = $request->session()->get('cart');

        $input['customer_id'] = 1;
        $input['date'] = date('Y-m-d');
        $input['total'] = 0;
        $order = $this->model_order->create($input);

        $total = 0;
        foreach($carts as $key => $value) {
            $product = $this->model_product->whereId($key)->first();

            $detail['order_id'] = $order->id;
            $detail['product_id'] = $product->id;
            $detail['price'] = $product->selling_price;
            $detail['qty'] = $value;
            $detail['subtotal'] = $product->selling_price * $value;
            $this->model_detail_order->create($detail);

            $total += $detail['subtotal'];
        }

        $order->update(['total' => $total]);

        return redirect()->route($this->view.'.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->model->findOrFail($id);

        return view('pages.' .$this->view . '.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->model->findOrFail($id);

        return view('pages.' .$this->view . '.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $data = $this->model->findOrFail($id);
        $data->update($input);

        return redirect()->route($this->view . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data= $this->model->findOrFail($id);
        $data->delete();

        return redirect()->route($this->view . '.index');
    }
}
