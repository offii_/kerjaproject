<?php

namespace App\Http\Controllers;
use App\Product;
use App\Unit;
use App\Category;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $title,$view;
    
    public function __construct(
        Product $model
    ){
        $this->model = $model;

        $this->title    = "Products";
        $this->view     = "product";

        view()->share('title', $this->title);
        view()->share('view', $this->view);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = $this->model->with(['category', 'unit'])->paginate(10);
        return view($this->view.'.index', compact('datas'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $datas = $this->model->with(['category', 'unit'])->paginate(10);
        $categories = Category::get();
        $units = Unit::all();
        return view($this->view.'.create', compact('datas','categories','units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $product = new Product($request->all());
        $product->name = $request->name;
        $product->image = $request->image;
        $product->category_id = $request->category;
        $product->purchase_price = $request->pprice;
        $product->selling_price = $request->sprice;
        $product->stock = $request->stock;
        $product->description = $request->description;
        $product->unit_id = $request->unit;
        $product->status = $request->status;
        $product->save();
        return redirect()->route($this->view.'.index')->with('message','success');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->model->findOrFail($id);
        
        return view($this->view. '.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $units = Unit::all();
        $categories = Category::all();
        $datas = $this->model->findOrFail($id);

        return view($this->view . '.edit', compact('datas','categories','units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Product::where('id',$id)->update([
            'name' => $request['name'],
            'image' => $request['image'],
            'category_id' => $request['category'],
            'purchase_price' => $request['pprice'],
            'selling_price' => $request['sprice'],
            'stock' => $request['stock'],
            'description' => $request['description'],
            'unit_id' => $request['unit'],
            'status' => $request['status']
        ]);
        
        return redirect()->route($this->view . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->model->findOrFail($id);
        $data->delete();
        return redirect()->route($this->view. '.index');
    }

   
}
