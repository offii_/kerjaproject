<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $title, $view;

    public function __construct(
        Category $model
    ) {
        $this->model = $model;

        $this->title = "Categories";
        $this->view = "category";

        view()->share('title', $this->title);
        view()->share('view', $this->view);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = $this->model->paginate(10);

        return view($this->view . '.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datas = Category::paginate(10);
        return view('category.create', compact('datas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $input = $request->all();
        // unset($input['_token']);

        // $this->model->create($input);

        // return redirect()->route($this->view . '.index');
        $category = new Category($request->all());
        $category->parent_id = $request->parent_id;
        $category->name = $request->name;
        $category->save();

        return redirect()->route('category.index')->with('message','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->model->findOrFail($id);

        return view( $this->view . '.show', compact('data'));
    }

    public function edit($id)
    {
        $data = $this->model->findOrFail($id);

        return view($this->view . '.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $input = $request->all();

        // $data = $this->model->findOrFail($id);
        // $data->update($input);

        // return redirect()->route($this->view . '.index');

        Category::where('id',$id)->update([
            'parent_id' => $request->parent_id,
            'name' => $request->name
        ]);
        
        return redirect()->route($this->view . '.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data= $this->model->findOrFail($id);
        $data->delete();

        return redirect()->route($this->view . '.index');
    }
}
