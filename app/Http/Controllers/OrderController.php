<?php

namespace App\Http\Controllers;
use Auth;
use App\Order;
use App\DetailOrder;
use App\Meja;
use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\OrderExport;

class OrderController extends Controller
{
    private $title,$view;

    public function __construct(
        Order $model
    ) {
        $this->model = $model;

        $this->title    = "Transacion";
        $this->view     = "order";

        view()->share('title', $this->title);
        view()->share('view', $this->view);
      }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = $this->model->paginate(10);
        $tables = Meja::where('active','1')->get();
        return view($this->view.'.index', compact('datas','tables'));
    }

    public function export_excel()
	{
		return Excel::download(new OrderExport, 'order.xlsx');
    }
    
    public function cart($id)
    {
        $datas = $this->model->where('id',$id)->get();
        $carts = DetailOrder::select('order_details.id as order_id','order_details.order_id as orders_id', 'order_details.qty','products.*')
        ->where('order_details.order_id',$id)
        ->leftJoin('products','order_details.product_id','=','products.id')
        ->get();
        $total_price = $cart = DetailOrder::where('order_id',$id)->sum('order_details.subtotal');
        $total_item = $cart = DetailOrder::where('order_id',$id)->sum('order_details.qty');
        return view('order.cart', compact('datas','carts','total_price','total_item'));
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $order = new Order($request->all());
        $order->customer_name = $request->name;
        $order->table_number = $request->table;
        $order->save();
        Meja::where('id',$request->table)->update([
            'active' => '0'
        ]);
        $customer_id = $this->model->max('id');
        $customer = $this->model->where('id', '=', $customer_id)->get();
        $categories = Category::select('id','name')->get()->toArray();
        
		// $product = [];
		// foreach ($categories as $keys => $c) {
		// 	$product[$keys] = $c;
        //     $product[$keys]['details'] = Product::where('category_id',$c['id'])
        //                          ->where('status',1)
		// 						 ->select('products.id',
        //                                     'products.name',
        //                                     'image',
        //                                     'selling_price',
		// 						 			'description'
		// 						 		)
		// 						 ->leftJoin('units','units.id','=','products.unit_id')
		// 						 ->get()->toArray();

        // }
        $product = Product::where('status',1)->get();
        return view('shop.index', compact('customer','categories','product'));
    }

    public function checkout(Request $request, $id)
    {
        Order::where('id',$id)->update([
            'total_item' => $request->total_item,
            'total_price' => $request->price,
            'cash' => $request->cash,
            'change' => $request->change,
            'status' => 0
        ]);
        
        return redirect()->route('order.index');
        // dd($request);
    }

    public function back()
    {
        $customer_id = $this->model->max('id');
        $customer = $this->model->where('id', '=', $customer_id)->get();
        $categories = Category::select('id','name')->get()->toArray();
        $product = Product::where('status',1)->get();
        
        return view('shop.index', compact('customer','categories','product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DetailOrder::where('order_details.id',$id)
        ->select('order_details.id','order_id','product_id','price','qty','subtotal','category_id','name','description','image','purchase_price','selling_price','stock','unit_id','status')
        ->rightJoin('products','order_details.product_id','=','products.id')->get();

        return view($this->view . '.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = DetailOrder::select('order_id')->where('id',$id)->first();
        $order_id = $order['order_id'];
        $qty = $request->quantity;
        $price = $request->price;
        $subtotal = $qty * $price;
        DetailOrder::where('id',$id)->update([
            'qty' => $qty,
            'subtotal' => $subtotal
        ]);
        
        return redirect()->route('cart.index', $order_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = DetailOrder::select('order_id')->where('id',$id)->first();
        $order_id = $order['order_id'];
        $data= DetailOrder::findOrFail($id);
        $data->delete();

        return redirect()->route('cart.index', $order_id);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
