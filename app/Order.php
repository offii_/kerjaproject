<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['customer_name', 'total', 'total_item', 'total_price', 'cash','change'];

    public function detail_orders()
    {
        return $this->hasMany('App\DetailOrder', 'detail_orders_id');

    }
}
