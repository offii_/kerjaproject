<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['unit_id','name','category_id','price'];

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');

    }

    public function unit()
    {
    	return $this->belongsTo('App\Unit', 'unit_id', 'id');
    }
}
