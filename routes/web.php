<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::group(['prefix' => 'isi', 'namespace' => 'Isi', 'as' => 'isi.', 'middleware' => 'web'], function ()
// {
    
// });

Route::get('menu', 'MenuController@index')->name('menu.index');
Route::get('menu', 'MenuController@show')->name('menu.show');

Route::get('cart', 'CartController@index')->name('cart.index');

Route::get('about', 'AboutController@index')->name('about.index');

Route::get('blog', 'BlogController@index')->name('blog.index');
Route::get('singleblog', 'BlogController@index')->name('singleblog.index');

Route::get('checkout', 'CheckoutController@index')->name('checkout.index');

Route::get('contact', 'ContactController@index')->name('contact.index');

Route::get('home', 'IndexController@index')->name('home.index');

Route::get('/', 'OrderController@index')->name('order.index');
Route::get('/export_excel', 'OrderController@export_excel')->name('order.export');
Route::post('order/create', 'OrderController@create')->name('order.create');
Route::get('order/create', 'OrderController@back')->name('order.creates');
Route::get('/shop/{id}', 'ShopController@store')->name('shop.store');
Route::post('order/store', 'ShopController@create')->name('shop.create');
Route::get('order/cart/{id}', 'OrderController@cart')->name('cart.index');
Route::get('order/{id}/edit', 'OrderController@edit')->name('order.edit');
Route::post('order/{id}/update', 'OrderController@update')->name('order.update');
Route::delete('order/{id}/delete', 'OrderController@destroy')->name('order.destroy');
Route::post('order/{id}/checkout', 'OrderController@checkout')->name('order.checkout');

Route::get('wishlist', 'WishlistController@index')->name('wishlist.index');

Route::get('unit/', 'UnitController@index')->name('unit.index');
Route::get('unit/create', 'UnitController@create')->name('unit.create');
Route::post('unit/store', 'UnitController@store')->name('unit.store');
Route::get('/unit/{id}', 'UnitController@show')->name('unit.show');
Route::get('/unit/{id}/edit', 'UnitController@edit')->name('unit.edit');
Route::put('/unit/{id}/update', 'UnitController@update')->name('unit.update');
Route::delete('/unit/{id}/delete', 'UnitController@destroy')->name('unit.destroy');

Route::get('category', 'CategoryController@index')->name('category.index');
Route::get('category/create', 'CategoryController@create')->name('category.create');
Route::post('category/store', 'CategoryController@store')->name('category.store');
Route::get('category/{id}', 'CategoryController@show')->name('category.show');
Route::get('category/{id}/edit', 'CategoryController@edit')->name('category.edit');
Route::put('category/{id}/update', 'CategoryController@update')->name('category.update');
Route::delete('category/{id}/delete', 'CategoryController@destroy')->name('category.destroy');

Route::get('product', 'ProductController@index')->name('product.index');
Route::get('product/create', 'ProductController@create')->name('product.create');
Route::post('product/store', 'ProductController@store')->name('product.store');
Route::get('product/{id}', 'ProductController@show')->name('product.show');
Route::get('product/{id}/edit', 'ProductController@edit')->name('product.edit');
Route::put('product/{id}/update', 'ProductController@update')->name('product.update');
Route::delete('product/{id}/delete', 'ProductController@destroy')->name('product.destroy');

Route::get('meja/', 'MejaController@index')->name('meja.index');
Route::get('meja/create', 'MejaController@create')->name('meja.create');
Route::post('meja/store', 'MejaController@store')->name('meja.store');
Route::get('meja/{id}', 'MejaController@show')->name('meja.show');
Route::get('meja/{id}/edit', 'MejaController@edit')->name('meja.edit');
Route::put('meja/{id}/update', 'MejaController@update')->name('meja.update');
Route::delete('meja/{id}/delete', 'MejaController@destroy')->name('meja.destroy');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
        
    Route::get('menu', 'MenuController@index')->name('menu.index');
    Route::get('menu', 'MenuController@show')->name('menu.show');

    Route::get('cart', 'CartController@index')->name('cart.index');

    Route::get('about', 'AboutController@index')->name('about.index');

    Route::get('blog', 'BlogController@index')->name('blog.index');
    Route::get('singleblog', 'BlogController@index')->name('singleblog.index');

    Route::get('checkout', 'CheckoutController@index')->name('checkout.index');

    Route::get('contact', 'ContactController@index')->name('contact.index');

    Route::get('home', 'IndexController@index')->name('home.index');

    Route::get('/', 'OrderController@index')->name('order.index');
    Route::post('order/create', 'OrderController@create')->name('order.create');
    Route::get('shop', 'ShopController@index')->name('shop.index');

    Route::get('wishlist', 'WishlistController@index')->name('wishlist.index');

    Route::get('unit/', 'UnitController@index')->name('unit.index');
    Route::get('unit/create', 'UnitController@create')->name('unit.create');
    Route::post('unit/store', 'UnitController@store')->name('unit.store');
    Route::get('/unit/{id}', 'UnitController@show')->name('unit.show');
    Route::get('/unit/{id}/edit', 'UnitController@edit')->name('unit.edit');
    Route::put('/unit/{id}/update', 'UnitController@update')->name('unit.update');
    Route::delete('/unit/{id}/delete', 'UnitController@destroy')->name('unit.destroy');

    Route::get('category', 'CategoryController@index')->name('category.index');
    Route::get('category/create', 'CategoryController@create')->name('category.create');
    Route::post('category/store', 'CategoryController@store')->name('category.store');
    Route::get('category/{id}', 'CategoryController@show')->name('category.show');
    Route::get('category/{id}/edit', 'CategoryController@edit')->name('category.edit');
    Route::put('category/{id}/update', 'CategoryController@update')->name('category.update');
    Route::delete('category/{id}/delete', 'CategoryController@destroy')->name('category.destroy');

    Route::get('product', 'ProductController@index')->name('product.index');
    Route::get('product/create', 'ProductController@create')->name('product.create');
    Route::post('product/store', 'ProductController@store')->name('product.store');
    Route::get('product/{id}', 'ProductController@show')->name('product.show');
    Route::get('product/{id}/edit', 'ProductController@edit')->name('product.edit');
    Route::put('product/{id}/update', 'ProductController@update')->name('product.update');
    Route::delete('product/{id}/delete', 'ProductController@destroy')->name('product.destroy');

    Route::get('meja/', 'MejaController@index')->name('meja.index');
    Route::get('meja/create', 'MejaController@create')->name('meja.create');
    Route::post('meja/store', 'MejaController@store')->name('meja.store');
    Route::get('meja/{id}', 'MejaController@show')->name('meja.show');
    Route::get('meja/{id}/edit', 'MejaController@edit')->name('meja.edit');
    Route::put('meja/{id}/update', 'MejaController@update')->name('meja.update');
    Route::delete('meja/{id}/delete', 'MejaController@destroy')->name('meja.destroy');
});

Route::get('/logout','OrderController@logout');